/*
 *  Copyright 2013 Lolmewn .
 */
package nl.lolmewn.achievements.player;

import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import nl.lolmewn.achievements.Main;
import nl.lolmewn.achievements.api.AchievementPlayerLoadedEvent;
import org.bukkit.OfflinePlayer;
import org.bukkit.configuration.file.YamlConfiguration;

/**
 *
 * @author Lolmewn 
 */
public class PlayerManager {

    private final Main plugin;
    private final ConcurrentHashMap<UUID, AchievementPlayer> players = new ConcurrentHashMap<>();
    private final HashSet<UUID> beingLoaded = new HashSet<>();
    private final YamlConfiguration conf;

    public PlayerManager(Main m) throws IOException {
        this.plugin = m;
        File ids = new File(m.getDataFolder(), "user-ids.yml");
        if (!ids.exists()) {
            ids.createNewFile();
        }
        this.conf = YamlConfiguration.loadConfiguration(ids);
    }

    public void loadPlayer(UUID uuid) {
        OfflinePlayer player = plugin.getServer().getOfflinePlayer(uuid);
        if (players.containsKey(player.getUniqueId())) {
            return;
        }
        if (this.beingLoaded.contains(player.getUniqueId())) {
            return;
        }
        this.beingLoaded.add(player.getUniqueId());
        AchievementPlayer aPlayer = new AchievementPlayer(uuid, player.getName());
        try {
            Connection con = this.plugin.getMysqlProvider().getConnection();
            PreparedStatement st = con.prepareStatement("SELECT * FROM "
                    + this.plugin.getMysqlProvider().getPrefix() + "achievements"
                    + " WHERE player_id=?");
            st.setInt(1, getPlayerId(uuid));
            ResultSet set = st.executeQuery();
            if (set != null) {
                while (set.next()) {
                    int completed = set.getInt("achievement_id");
                    aPlayer.markAsCompleted(completed);
                }
                set.close();
            }
            st.close();
        } catch (SQLException ex) {
            Logger.getLogger(PlayerManager.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            this.beingLoaded.remove(player.getUniqueId());
            this.players.put(player.getUniqueId(), aPlayer);
            AchievementPlayerLoadedEvent event = new AchievementPlayerLoadedEvent(aPlayer);
            this.plugin.getServer().getPluginManager().callEvent(event);
            if (!conf.contains(player.getUniqueId().toString())) {
                this.conf.set(player.getUniqueId().toString(), getPlayerId(player.getUniqueId()));
                try {
                    this.conf.save(new File(this.plugin.getDataFolder(), "user-ids.yml"));
                } catch (IOException ex) {
                    Logger.getLogger(PlayerManager.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    }

    public AchievementPlayer getPlayer(String name) {
        OfflinePlayer player = plugin.getServer().getOfflinePlayer(name);
        if (!this.players.containsKey(player.getUniqueId())) {
            return null;
        }
        return this.players.get(player.getUniqueId());
    }

    public AchievementPlayer getPlayer(UUID uuid) {
        return this.players.get(uuid);
    }

    public void savePlayer(final UUID uuid, final boolean remove) {
        final AchievementPlayer player = this.getPlayer(uuid);
        if (player == null) {
            return;
        }
        final int id = this.getPlayerId(uuid);
        try {
            Connection con = this.plugin.getMysqlProvider().getConnection();

            PreparedStatement presentCheck = con.prepareStatement("SELECT * FROM " + this.plugin.getMysqlProvider().getPrefix() + "achievements WHERE player_id=? AND achievement_id=?");
            PreparedStatement st = con.prepareStatement("INSERT INTO " + this.plugin.getMysqlProvider().getPrefix() + "achievements (player_id, achievement_id) VALUES (?, ?)");
            presentCheck.setInt(1, id);
            st.setInt(1, id);

            boolean anyInsert = false;

            for (int completed : player.getCompletedAchievements()) {
                presentCheck.setInt(2, completed);
                ResultSet set = presentCheck.executeQuery();
                if (!set.next()) {
                    st.setInt(2, completed);
                    st.addBatch();
                    if (!anyInsert) {
                        con.setAutoCommit(false);
                        anyInsert = true;
                    }
                }
                set.close();
            }

            if (anyInsert) {
                st.executeBatch();
                con.commit();
            }
            st.close();
            presentCheck.close();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(PlayerManager.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (remove) {
                removePlayer(uuid);
            }
        }
    }

    public Collection<AchievementPlayer> getAchievementPlayers() {
        return this.players.values();
    }

    public Set<UUID> getPlayers() {
        return this.players.keySet();
    }

    public void removePlayer(String name) {
        OfflinePlayer op = plugin.getServer().getOfflinePlayer(name);
        this.removePlayer(op.getUniqueId());
    }

    public void removePlayer(UUID uuid) {
        this.players.remove(uuid);
    }

    int lastId = 1;
    private int getPlayerId(UUID uuid) {
        if (this.plugin.hasStats()) {
            return plugin.getStatsAPI().getPlayerId(uuid);
        } else {
            if(this.conf.contains(uuid.toString())){
                return this.conf.getInt(uuid.toString());
            }
            for(; conf.contains("" + lastId); lastId++){}
            return lastId+1;
        }
    }
}
