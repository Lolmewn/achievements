/*
 *  Copyright 2013 Lolmewn .
 */

package nl.lolmewn.achievements.player;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 *
 * @author Lolmewn 
 */
public class AchievementPlayer {    
    
    private final String name;
    private final UUID uuid;
    private final List<Integer> completedAchievements = new ArrayList<>();
    
    public AchievementPlayer(UUID uuid, String name) {
        this.name = name;
        this.uuid = uuid;
    }
    
    public String getName(){
        return name;
    }

    public UUID getUuid() {
        return uuid;
    }
    
    public boolean hasCompletedAchievement(int id){
        return this.completedAchievements.contains(id);
    }
    
    public void markAsCompleted(int id){
        this.completedAchievements.add(id);
    }

    public List<Integer> getCompletedAchievements() {
        return this.completedAchievements;
    }

}
