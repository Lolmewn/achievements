/*
 *  Copyright 2013 Lolmewn .
 */
package nl.lolmewn.achievements;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import nl.lolmewn.achievements.api.AchievementAdapter;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;

/**
 *
 * @author Lolmewn 
 */
public class AchievementManager {

    private final Main plugin;
    private final HashMap<Integer, Achievement> achievements = new HashMap<>();
    private int nextFreeId;
    private YamlConfiguration c;
    private File file;

    private boolean needsSync = false; // Sync ID with name in DB

    public AchievementManager(Main aThis) {
        plugin = aThis;
    }

    public void loadAchievements() throws InvalidConfigurationException {
        if (c == null) {
            file = new File(plugin.getDataFolder(), "achievements.yml");
            if (!file.exists()) {
                plugin.saveResource("achievements.yml", true);
                return;
            }
            c = YamlConfiguration.loadConfiguration(file);
        }
        try {
            c.load(file);
        } catch (IOException ex) {
            Logger.getLogger(AchievementManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        for (String key : c.getConfigurationSection("").getKeys(false)) {
            try {
                int id = Integer.parseInt(key);
                ConfigurationSection section = c.getConfigurationSection(key);
                Achievement ach = new Achievement(plugin, id);
                if (this.load(ach, section)) {
                    this.addAchievement(id, ach);
                    calculateNextFreeId(id);
                }
            } catch (NumberFormatException e) {
                plugin.getLogger().warning("Failed to load achievement, ID was not an int: " + key);
            }
        }
    }

    public void addAchievement(int id, Achievement a) {
        if (this.achievements.containsKey(id)) {
            plugin.getLogger().warning("Duplicate ID found for achievements, "
                    + "you can only use an ID once. ID: " + id + ", achievement trying to add (failing): " + a.getName());
        }
        this.achievements.put(id, a);
        this.needsSync = true;
    }

    public void clearAchievements() {
        this.achievements.clear();
    }

    public Collection<Achievement> getAchievements() {
        return achievements.values();
    }

    private void calculateNextFreeId(int id) {
        while (this.achievements.containsKey(id)) {
            id++;
        }
        this.nextFreeId = id;
    }

    public int getNextFreeId() {
        this.calculateNextFreeId(nextFreeId);
        return this.nextFreeId;
    }

    public Achievement findAchievement(String q) {
        try {
            int i = Integer.parseInt(q);
            return this.achievements.get(i);
        } catch (NumberFormatException e) {
            for (Achievement ach : this.getAchievements()) {
                if (ach.getName().toLowerCase().startsWith(q.toLowerCase())) {
                    return ach;
                }
            }
            for (Achievement ach : this.getAchievements()) {
                if (ach.getName().toLowerCase().contains(q.toLowerCase())) {
                    return ach;
                }
            }
        }
        return null;
    }

    public boolean load(Achievement ach, ConfigurationSection loadFrom) {
        ach.setName(loadFrom.getString("name"));
        ach.setDescription(loadFrom.getString("description", "The " + ach.getName() + " achievement."));
        if (loadFrom.contains("goals")) {
            this.loadGoals(ach, loadFrom.getConfigurationSection("goals"));
        }
        if (loadFrom.contains("rewards")) {
            loadRewards(ach, loadFrom.getConfigurationSection("rewards"));
        }
        if (loadFrom.contains("onComplete")) {
            loadOnComplete(ach, loadFrom.getConfigurationSection("onComplete"));
        }
        return true;
    }

    private void loadGoals(Achievement ach, ConfigurationSection loadFrom) {
        for (String key : loadFrom.getKeys(false)) {
            for (AchievementAdapter adap : this.plugin.getAPI().getAdapters()) {
                if (adap.hasGoal(key, loadFrom.getConfigurationSection(key))) {
                    ach.addGoal(adap.getGoal(key, loadFrom.getConfigurationSection(key) == null ? loadFrom : loadFrom.getConfigurationSection(key)));
                }
            }
        }
    }

    private void loadRewards(Achievement ach, ConfigurationSection loadFrom) {
        for (String key : loadFrom.getKeys(false)) {
            for (AchievementAdapter adap : this.plugin.getAPI().getAdapters()) {
                if (adap.hasReward(key)) {
                    ach.addReward(adap.getReward(key, loadFrom.getConfigurationSection(key) == null ? loadFrom : loadFrom.getConfigurationSection(key)));
                }
            }
        }
    }

    private void loadOnComplete(Achievement ach, ConfigurationSection loadFrom) {
        for (String key : loadFrom.getKeys(false)) {
            for (AchievementAdapter adap : this.plugin.getAPI().getAdapters()) {
                if (adap.hasCompletion(key)) {
                    ach.addCompletion(adap.getCompletion(key, loadFrom.getConfigurationSection(key) == null ? loadFrom : loadFrom.getConfigurationSection(key)));
                }
            }
        }
    }

    public boolean isNeedingSync() {
        return needsSync;
    }
}
