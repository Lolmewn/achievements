/*
 *  Copyright 2013 Lolmewn .
 */

package nl.lolmewn.achievements.goal;

import nl.lolmewn.achievements.api.Goal;
import java.util.Arrays;
import nl.lolmewn.achievements.player.AchievementPlayer;
import nl.lolmewn.stats.api.Stat;
import nl.lolmewn.stats.api.StatsAPI;
import nl.lolmewn.stats.player.StatData;
import nl.lolmewn.stats.player.StatsPlayer;
import org.bukkit.Material;

/**
 *
 * @author Lolmewn 
 */
public class StatsGoal implements Goal {

    private final StatsAPI api;
    private final String goalType;
    private final Stat stat;
    private final double amount;
    private boolean global;
    private Object[] variables;
    
    public StatsGoal(StatsAPI api, Stat stat, double amount){
        this.api = api;
        this.goalType = "Stats";
        this.amount = amount;
        this.stat = stat;
    }
    
    public StatsGoal(StatsAPI api, Stat stat, double amount, boolean global, Object[] vars){
        this(api, stat, amount);
        this.setGlobal(global);
        this.setVariables(vars);
    }

    public double getAmount() {
        return amount;
    }

    public boolean isGlobal() {
        return global;
    }
    
    public void setGlobal(boolean yes){
        this.global = yes;
    }
    
    @Override
    public String getType(){
        return goalType;
    }
    
    public Stat getStat(){
        return this.stat;
    }

    public Object[] getVariables() {
        return this.variables;
    }
    
    public void setVariables(Object[] vars){
        this.variables = vars;
    }

    @Override
    public String getDisplayName() {
        return stat.getName();
    }

    @Override
    public String getDisplayObjective() {
        if(stat.getName().equalsIgnoreCase("move")){
            String type;
            if(this.isGlobal()){
                type = "in total";
            }else{
                switch(Integer.parseInt(this.getVariables()[0].toString())){
                    case 1:
                        type = "by boat";
                        break;
                    case 2:
                        type = "by train";
                        break;
                    case 3:
                        type = "on a pig";
                        break;
                    case 4:
                        type = "on a pig in a train";
                        break;
                    case 5:
                        type = "by horse";
                        break;
                    default:
                        type = "by foot";
                }
            }
            return this.getAmount() + " blocks " + type;
        }
        if(stat.getName().toLowerCase().startsWith("block")){
            if(this.isGlobal()){
                return this.getAmount() + " in total";
            }
            try{
                int id = Integer.parseInt((String)this.getVariables()[0]);
                return this.getAmount() + " " + Material.getMaterial(id).toString().toLowerCase().replace("_", " ");
            }catch(NumberFormatException nfe){
                return this.getAmount() + " " + Material.valueOf((String)this.getVariables()[0]).toString().toLowerCase().replace("_", "");
            }
        }
        if(stat.getName().toLowerCase().equals("kill")){
            if(this.isGlobal()){
                return this.getAmount() + " entities in total";
            }
            return this.getAmount() + " " + (String)this.getVariables()[0];
        }
        if(stat.getName().toLowerCase().equals("death")){
            if(this.isGlobal()){
                return this.getAmount() + " times ";
            }
            return this.getAmount() + " times by " + (String)this.getVariables()[0];
        }
        return this.getAmount() + " " + stat.getName();
    }

    @Override
    public double getProgress(AchievementPlayer player) {
        StatsPlayer sPlayer = api.getPlayer(player.getUuid());
        StatData globalData = sPlayer.getGlobalStatData(getStat());
        double count = 0;
        if (isGlobal()) {
            for (Object[] vars : globalData.getAllVariables()) {
                count += globalData.getValue(vars);
            }
        } else {
            count = globalData.getValue(getVariables());
        }
        return (count / getAmount());
    }

}
