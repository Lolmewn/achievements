package nl.lolmewn.achievements.completion;

import java.util.Arrays;
import mkremins.fanciful.FancyMessage;
import nl.lolmewn.achievements.Achievement;
import nl.lolmewn.achievements.api.Completion;
import nl.lolmewn.achievements.player.AchievementPlayer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

/**
 * @author Sybren
 */
public class Broadcast implements Completion {

    private final String message;

    public Broadcast(String message) {
        this.message = message;
    }

    @Override
    public void onCompletion(AchievementPlayer player, Achievement ach) {
        if (!message.contains("%achievement%")) {
            Bukkit.getServer().broadcastMessage(colorise(message.replace("%player%", player.getName())));
        } else {
            new FancyMessage(colorise(getBeforeAchiev(message).replace("%player%", player.getName())))
                    .then(colorise(ach.getName()))
                    .style(ChatColor.UNDERLINE)
                    .tooltip(colorise(ach.getName()), ach.getDescription())
                    .then(colorise(this.getAfterAchiev(message)))
                    .send(Arrays.asList(Bukkit.getServer().getOnlinePlayers()));
        }
    }

    public String getBeforeAchiev(String msg) {
        return msg.split("%achievement%")[0];
    }

    public String getAfterAchiev(String msg) {
        String[] split = msg.split("%achievement%");
        return split.length == 1 ? "" : split[1];
    }

    public String colorise(String msg) {
        return ChatColor.translateAlternateColorCodes('&', msg);
    }

}
