package nl.lolmewn.achievements.completion;

import java.util.List;
import nl.lolmewn.achievements.Achievement;
import nl.lolmewn.achievements.api.Completion;
import nl.lolmewn.achievements.player.AchievementPlayer;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

/**
 * @author Sybren
 */
public class Messages implements Completion {

    private final List<String> messages;

    public Messages(List<String> messages) {
        this.messages = messages;
    }

    @Override
    public void onCompletion(AchievementPlayer player, Achievement ach) {
        OfflinePlayer op = Bukkit.getOfflinePlayer(player.getUuid());
        if (!op.isOnline()) {
            return;
        }
        Player p = (Player) op;
        for (String message : messages) {
            p.sendMessage(message.replace("%player%", player.getName()).replace("%name%", ach.getName()));
        }
    }

}
