package nl.lolmewn.achievements.reward;

import nl.lolmewn.achievements.api.Reward;
import nl.lolmewn.achievements.player.AchievementPlayer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * @author Sybren
 */
public class Items implements Reward {

    private final String items;

    public Items(String items) {
        this.items = items;
    }

    @Override
    public void give(AchievementPlayer player) {
        OfflinePlayer op = Bukkit.getOfflinePlayer(player.getUuid());
        if(!op.isOnline()){
            return;
        }
        Player p = (Player)op;
        String item = items.split(",")[0];
        int amount = Integer.parseInt(items.split(",")[1]);
        ItemStack stack;
        if (item.contains(".")) {
            stack = new ItemStack(Material.getMaterial(Integer.parseInt(item.split("\\.")[0])), amount, Short.parseShort(item.split("\\.")[1]));
        } else {
            stack = new ItemStack(Material.getMaterial(Integer.parseInt(item)), amount);
        }
        if (!p.getInventory().addItem(stack).isEmpty()) {
            p.getWorld().dropItem(p.getLocation(), stack);
            p.sendMessage(ChatColor.GREEN + "Inventory full, item dropped on the ground.");
        }
    }

}
