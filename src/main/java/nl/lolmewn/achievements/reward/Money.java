package nl.lolmewn.achievements.reward;

import net.milkbowl.vault.economy.Economy;
import nl.lolmewn.achievements.api.Reward;
import nl.lolmewn.achievements.player.AchievementPlayer;
import org.bukkit.OfflinePlayer;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.RegisteredServiceProvider;

/**
 * @author Sybren
 */
public class Money implements Reward {
    
    private final double amount;
    private final Plugin plugin;
    private Economy economy;

    public Money(double amount, Plugin plugin) {
        this.amount = amount;
        this.plugin = plugin;
    }

    public boolean setupEconomy() {
        if (economy != null) {
            return true;
        }
        if (plugin.getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> economyProvider = this.plugin.getServer().getServicesManager().getRegistration(net.milkbowl.vault.economy.Economy.class);
        if (economyProvider != null) {
            economy = economyProvider.getProvider();
        }
        return (economy != null);
    }

    @Override
    public void give(AchievementPlayer player) {
        if(!this.setupEconomy()){
            return;
        }
        OfflinePlayer op = plugin.getServer().getOfflinePlayer(player.getUuid());
        this.economy.depositPlayer(op, amount);
    }

}
