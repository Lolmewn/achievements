package nl.lolmewn.achievements.reward;

import java.util.List;
import nl.lolmewn.achievements.api.Reward;
import nl.lolmewn.achievements.player.AchievementPlayer;
import org.bukkit.Bukkit;

/**
 * @author Sybren
 */
public class ConsoleCommand implements Reward {

    private final List<String> commands;

    public ConsoleCommand(List<String> commands) {
        this.commands = commands;
    }

    @Override
    public void give(AchievementPlayer player) {
        for (String command : commands) {
            Bukkit.dispatchCommand(Bukkit.getConsoleSender(), command.replace("%player%", player.getName()));
        }
    }

}
