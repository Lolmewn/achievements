package nl.lolmewn.achievements.reward;

import java.util.List;
import nl.lolmewn.achievements.api.Reward;
import nl.lolmewn.achievements.player.AchievementPlayer;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

/**
 * @author Sybren
 */
public class Commands implements Reward {

    private final List<String> commands;

    public Commands(List<String> commands) {
        this.commands = commands;
    }

    @Override
    public void give(AchievementPlayer player) {
        OfflinePlayer op = Bukkit.getOfflinePlayer(player.getUuid());
        if (op.isOnline()) {
            Player p = (Player) op;
            for (String command : commands) {
                p.performCommand(command.replace("%player%", player.getName()));
            }
        }
    }

}
