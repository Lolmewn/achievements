package nl.lolmewn.achievements;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import java.util.Collection;
import java.util.UUID;
import nl.lolmewn.achievements.api.AchievementAdapter;
import nl.lolmewn.achievements.player.AchievementPlayer;
import org.bukkit.plugin.Plugin;

/**
 * @author Lolmewn
 */
public class AchievementsAPI {
    
    private final Main main;
    private final Multimap<Plugin, AchievementAdapter> adapters = ArrayListMultimap.create();
    
    protected AchievementsAPI(Main m){
        this.main = m;
    }
    
    public int findNextFreeId(){
        return main.getAchievementManager().getNextFreeId();
    }
    
    public void addAchievement(Achievement ach){
        main.getAchievementManager().addAchievement(ach.getId(), ach);
    }
    
    public void addAdapter(AchievementAdapter adapter){
        this.adapters.put(adapter.getPlugin(), adapter);
    }

    public Collection<AchievementAdapter> getAdapters() {
        return adapters.values();
    }
    
    public Collection<AchievementAdapter> getAdapter(Plugin plugin){
        return this.adapters.get(plugin);
    }
    
    public AchievementPlayer getPlayer(UUID uuid){
        return this.main.getPlayerManager().getPlayer(uuid);
    }
    
    public AchievementPlayer getPlayer(String name){
        return this.main.getPlayerManager().getPlayer(name);
    }
    
    public Achievement getAchievement(String name){
        return this.main.getAchievementManager().findAchievement(name);
    }

    public Collection<Achievement> getAchievements() {
        return this.main.getAchievementManager().getAchievements();
    }

}
