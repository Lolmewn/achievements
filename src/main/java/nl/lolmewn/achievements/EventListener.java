/*
 *  Copyright 2013 Lolmewn .
 */
package nl.lolmewn.achievements;

import java.util.UUID;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.server.PluginDisableEvent;

/**
 *
 * @author Lolmewn 
 */
public class EventListener implements Listener {

    private final Main plugin;
    
    public EventListener(Main m) {
        plugin = m;
    }

    @EventHandler
    public void onLogin(PlayerJoinEvent event) {
        plugin.getPlayerManager().loadPlayer(event.getPlayer().getUniqueId());
    }

    @EventHandler
    public void onLogout(final PlayerQuitEvent event) {
        final String name = event.getPlayer().getName();
        plugin.getServer().getScheduler().runTaskLater(plugin, new Runnable() {

            @Override
            public void run() {
                if (plugin.getServer().getPlayerExact(name) == null) {
                    plugin.getPlayerManager().removePlayer(name);
                }
            }
        }, 20L);
        plugin.getPlayerManager().savePlayer(event.getPlayer().getUniqueId(), false);
    }

    @EventHandler
    public void onDisable(PluginDisableEvent event) {
        if (event.getPlugin().equals(this.plugin)) {
            if (plugin.getPlayerManager() != null) {
                for (UUID player : plugin.getPlayerManager().getPlayers()) {
                    plugin.getPlayerManager().savePlayer(player, true);
                }
            }
        }
    }

}
