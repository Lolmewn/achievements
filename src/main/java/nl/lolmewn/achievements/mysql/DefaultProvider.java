package nl.lolmewn.achievements.mysql;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import nl.lolmewn.achievements.Main;

/**
 *
 * @author Lolmewn
 */
public class DefaultProvider implements MySQLProvider {

    private final Main plugin;
    private Connection connection;

    public DefaultProvider(Main plugin) {
        this.plugin = plugin;
    }

    @Override
    public Connection getConnection() {
        try {
            if (connection == null || !connection.isValid(0)) {
                if (connection != null && !connection.isClosed()) {
                    connection.close();
                }
                try {
                    connection = DriverManager.getConnection("jdbc:mysql://"
                            + plugin.getConfig().getString("mysql.host")
                            + "/"
                            + plugin.getConfig().getString("mysql.database"),
                            plugin.getConfig().getString("mysql.user"),
                            plugin.getConfig().getString("mysql.password"));
                } catch (SQLException ex) {
                    Logger.getLogger(DefaultProvider.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(DefaultProvider.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
        }
        return connection;
    }

    @Override
    public String getPrefix() {
        return plugin.getConfig().getString("mysql.prefix", "");
    }

    @Override
    public boolean hasTable(String name) throws SQLException {
        Connection con = this.getConnection();
        DatabaseMetaData dbm = con.getMetaData();
        ResultSet tables = dbm.getTables(null, null, name, null);
        if (tables.next()) {
            tables.close();
            return true;
        } else {
            tables.close();
            return false;
        }
    }

}
