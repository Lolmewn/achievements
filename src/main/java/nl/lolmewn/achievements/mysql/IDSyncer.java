package nl.lolmewn.achievements.mysql;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import nl.lolmewn.achievements.Achievement;
import nl.lolmewn.achievements.Main;
import nl.lolmewn.stats.api.mysql.MySQLAttribute;
import nl.lolmewn.stats.api.mysql.MySQLType;
import nl.lolmewn.stats.api.mysql.StatsColumn;
import nl.lolmewn.stats.api.mysql.StatsTable;

/**
 * @author Sybren
 */
public class IDSyncer implements Runnable {

    private final Main plugin;
    private StatsTable table;
    

    public IDSyncer(Main plugin) {
        this.plugin = plugin;
        table = plugin.getStatsAPI().getStatsTable("Ach_linker");
        if (table == null) {
            table = new StatsTable("Ach_linker", false, false);
            plugin.getStatsAPI().getStatsTableManager().put("Ach_linker", table);
        }
        table.addColumn("id", MySQLType.INTEGER).addAttributes(MySQLAttribute.PRIMARY_KEY, MySQLAttribute.NOT_NULL);
        table.addColumn("name", MySQLType.STRING);
    }

    @Override
    public void run() {
        if (!plugin.getAchievementManager().isNeedingSync()) {
            return;
        }
        Connection con = plugin.getStatsAPI().getConnection();
        for (final Achievement ach : plugin.getAchievementManager().getAchievements()) {
            try {
                table.getColumn("name").dbSet(con,
                        new ArrayList<StatsColumn>() {
                            {
                                this.add(table.getColumn("id"));
                            }
                        },
                        new ArrayList<Object>() {
                            {
                                this.add(ach.getId());
                            }
                        }, ach.getName());
            } catch (SQLException ex) {
                Logger.getLogger(IDSyncer.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        try {
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(IDSyncer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
