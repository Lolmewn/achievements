package nl.lolmewn.achievements.mysql;

import java.sql.Connection;
import java.sql.SQLException;

/**
 *
 * @author Lolmewn
 */
public interface MySQLProvider {
    
    public Connection getConnection() throws SQLException;
    public String getPrefix();
    public boolean hasTable(String name) throws SQLException;

}
