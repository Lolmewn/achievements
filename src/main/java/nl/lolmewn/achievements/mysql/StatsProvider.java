package nl.lolmewn.achievements.mysql;

import java.sql.Connection;
import nl.lolmewn.stats.api.StatsAPI;

/**
 *
 * @author Lolmewn
 */
public class StatsProvider implements MySQLProvider{
    
    private final StatsAPI api;

    public StatsProvider(StatsAPI api) {
        this.api = api;
    }

    @Override
    public Connection getConnection() {
        return api.getConnection();
    }

    @Override
    public String getPrefix() {
        return api.getDatabasePrefix();
    }

    @Override
    public boolean hasTable(String name) {
        return api.getStatsTableManager().containsKey(name);
    }

}
