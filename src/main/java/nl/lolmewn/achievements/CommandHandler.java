package nl.lolmewn.achievements;

import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import mkremins.fanciful.FancyMessage;
import nl.lolmewn.achievements.api.Goal;
import nl.lolmewn.achievements.player.AchievementPlayer;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.entity.Player;

/**
 * @author Sybren
 */
public class CommandHandler implements CommandExecutor {

    private final Main plugin;

    public CommandHandler(Main main) {
        this.plugin = main;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length == 0) {
            if (!(sender instanceof Player)) {
                sender.sendMessage("You're not a player, can't view your achievements!");
                return true;
            }
            if (sender.hasPermission("achievements.view.self")) {
                AchievementPlayer ap = plugin.getPlayerManager().getPlayer(sender.getName());
                this.sendAchievements(ap, sender, 1);
                return true;
            }
            sender.sendMessage("You do not have permissions to do this!");
            return true;
        }
        if (args[0].equalsIgnoreCase("help")) {

        }
        if (args[0].equalsIgnoreCase("player")) {
            if (args.length == 1) {
                sender.sendMessage(ChatColor.RED + "Correct usage: /" + label + " player <playerName>");
                return true;
            }
            String playerName = args[1];
            Player player = plugin.getServer().getPlayer(playerName);
            if (player == null) {
                sender.sendMessage(ChatColor.RED + "Player could not be found! Is he offline?");
                return true;
            }
            if (sender.hasPermission("achievements.view.others")) {
                AchievementPlayer ap = plugin.getPlayerManager().getPlayer(player.getName());
                int page;
                if (args.length == 2) {
                    page = 1;
                } else {
                    try {
                        page = Integer.parseInt(args[2]);
                    } catch (NumberFormatException e) {
                        sender.sendMessage(args[2] + " is not a number!");
                        return true;
                    }
                }
                this.sendAchievements(ap, sender, page);
            }
        }
        if (args[0].equalsIgnoreCase("reload")) {
            if (!sender.hasPermission("achievements.reload")) {
                sender.sendMessage("You do not have permissions to do this!");
                return true;
            }
            try {
                sender.sendMessage("Reloading achievements...");
                plugin.getAchievementManager().clearAchievements();
                plugin.getAchievementManager().loadAchievements();
                sender.sendMessage(ChatColor.GREEN + "All achievements have been succesfully reloaded");
                sender.sendMessage("Saving and loading all players...");
                for (UUID player : plugin.getPlayerManager().getPlayers()) {
                    plugin.getPlayerManager().savePlayer(player, true);
                    plugin.getPlayerManager().loadPlayer(player);
                }
                sender.sendMessage(ChatColor.GREEN + "All set and ready to roll!");
            } catch (InvalidConfigurationException ex) {
                Logger.getLogger(CommandHandler.class.getName()).log(Level.SEVERE, null, ex);
                sender.sendMessage(ChatColor.RED + "There seems to be a problem with your config file, please check the logs");
            }
        }
        if (args[0].equalsIgnoreCase("page")) {
            if (!(sender instanceof Player)) {
                sender.sendMessage("You're not a player, can't view your achievements!");
                return true;
            }
            if (!sender.hasPermission("achievements.view.self")) {
                sender.sendMessage("You do not have permissions to do this!");
                return true;
            }
            if (args.length == 1) {
                sender.sendMessage("Correct usage: /" + label + " page <pageNumber>");
                return true;
            }
            String numberString = args[1];
            try {
                int page = Integer.parseInt(numberString);
                this.sendAchievements(plugin.getPlayerManager().getPlayer(sender.getName()), sender, page);
            } catch (NumberFormatException e) {
                sender.sendMessage(args[1] + " is not a number!");
            }
            return true;
        }
        Achievement ach = plugin.getAchievementManager().findAchievement(args[0]);
        if (ach == null) {
            sender.sendMessage("Achievement or subcommand not found! Try /" + label + " help");
            return true;
        }
        sender.sendMessage("===" + ach.getName() + "===");
        if (ach.getDescription() != null && !ach.getDescription().equals("")) {
            sender.sendMessage(ach.getDescription());
        }
        AchievementPlayer ap = this.plugin.getPlayerManager().getPlayer(sender.getName());
        if (ap != null) {
            double completed = this.getPercentCompleted(ap, ach);
            StringBuilder sb = new StringBuilder();
            sb.append(ChatColor.RESET).append("[");
            for (int i = 0; i < completed / 10; i++) {
                sb.append(ChatColor.GREEN).append("|");
            }
            for (int i = 10 - (int) completed / 10; i > 0; i--) {
                sb.append(ChatColor.RED).append("|");
            }
            sb.append(ChatColor.RESET).append("]");
            sender.sendMessage("Completion: " + sb.toString());
        }
        StringBuilder sb = new StringBuilder();
        sb.append("Goals: ");
        for (Goal goal : ach.getGoals()) {
            sb.append(goal.getDisplayName()).append(": ").append(goal.getDisplayObjective());
        }
        sender.sendMessage(sb.toString());
        return true;
    }

    public void sendAchievements(AchievementPlayer aPlayer, CommandSender sender, int page) {
        sender.sendMessage(ChatColor.GREEN + "====Achievements====");
        sender.sendMessage(ChatColor.LIGHT_PURPLE + "Completed: "
                + ChatColor.GREEN + aPlayer.getCompletedAchievements().size()
                + ChatColor.LIGHT_PURPLE + "/"
                + ChatColor.RED + plugin.getAchievementManager().getAchievements().size());
        HashMap<Achievement, Double> map = orderByPercentageCompleted(aPlayer, plugin.getAchievementManager().getAchievements());
        SortedSet<Map.Entry<Achievement, Double>> sortedSet = this.entriesSortedByValues(map);
        int shown = 0;
        int index = 0;
        for (Map.Entry<Achievement, Double> entry : sortedSet) {
            double key = entry.getValue();
            if (key == 100) {
                continue;
            }
            if (!(page * 9 - 9 <= index && page * 9 > index)) {
                index++;
                continue;
            }
            Achievement value = entry.getKey();
            StringBuilder sb = new StringBuilder();
            sb.append(" [");
            for (int i = 0; i < key / 2; i++) {
                sb.append(ChatColor.GREEN).append("|");
            }
            for (int i = 50 - (int) key / 2; i > 0; i--) {
                sb.append(ChatColor.RED).append("|");
            }
            sb.append(ChatColor.RESET).append("]");
            new FancyMessage(value.getName())
                    .color(ChatColor.LIGHT_PURPLE)
                    .style(ChatColor.ITALIC)
                    .tooltip(value.getName(), value.getDescription())
                    .then(sb.toString())
                    .send(sender);
            //sender.sendMessage(sb.toString());
            if (++shown > 8) {
                break;
            }
        }
        if (shown == 0) {
            sender.sendMessage("There is no page " + page + " available!");
        }
    }

    private HashMap<Achievement, Double> orderByPercentageCompleted(AchievementPlayer ap, Collection<Achievement> achievements) {
        HashMap<Achievement, Double> map = new HashMap<>();
        for (Achievement ach : achievements) {
            map.put(ach, this.getPercentCompleted(ap, ach));
        }
        return map;
    }

    public double getPercentCompleted(AchievementPlayer player, Achievement ach) {
        double percent = -1;
        int amount = 0;
        for (Goal goal : ach.getGoals()) {
            if (percent == -1) {
                //first time
                percent = this.getGoalCompletion(player, goal);
                amount++;
            } else {
                percent = ((this.getGoalCompletion(player, goal)) + percent * amount) / (amount + 1);
                amount++;
            }
        }
        return percent > 100 ? 100 : percent;
    }

    public double getGoalCompletion(AchievementPlayer player, Goal goal) {
        return goal.getProgress(player) * 100;
    }

    public <Achievement, Double extends Comparable<? super Double>> SortedSet<Map.Entry<Achievement, Double>> entriesSortedByValues(HashMap<Achievement, Double> originalMap) {
        Map<Achievement, Double> map = (Map<Achievement, Double>) originalMap;
        SortedSet<Map.Entry<Achievement, Double>> sortedEntries = new TreeSet<>(
                new Comparator<Map.Entry<Achievement, Double>>() {

                    @Override
                    public int compare(Map.Entry<Achievement, Double> e1, Map.Entry<Achievement, Double> e2) {
                        int res = e2.getValue().compareTo(e1.getValue());
                        if (e1.getKey().equals(e2.getKey())) {
                            return res; // Code will now handle equality properly
                        } else {
                            return res != 0 ? res : 1; // While still adding all entries
                        }
                    }
                });
        sortedEntries.addAll(map.entrySet());
        return sortedEntries;
    }

}
