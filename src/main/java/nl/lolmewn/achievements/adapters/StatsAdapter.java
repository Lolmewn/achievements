package nl.lolmewn.achievements.adapters;

import nl.lolmewn.achievements.AchievementsAPI;
import nl.lolmewn.achievements.api.AchievementAdapter;
import nl.lolmewn.achievements.api.Completion;
import nl.lolmewn.achievements.api.Goal;
import nl.lolmewn.achievements.api.Reward;
import nl.lolmewn.achievements.goal.StatsGoal;
import nl.lolmewn.stats.api.Stat;
import nl.lolmewn.stats.api.StatsAPI;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.plugin.Plugin;

/**
 * @author Lolmewn
 */
public class StatsAdapter implements AchievementAdapter {

    private final Plugin plugin;
    private final StatsAPI sapi;
    private final AchievementsAPI aapi;

    public StatsAdapter(Plugin plugin, AchievementsAPI aapi, StatsAPI sapi) {
        this.plugin = plugin;
        this.sapi = sapi;
        this.aapi = aapi;
        plugin.getServer().getPluginManager().registerEvents(new StatsListener(plugin, aapi), plugin);
    }

    @Override
    public Plugin getPlugin() {
        return plugin;
    }

    @Override
    public AchievementsAPI getAPI() {
        return aapi;
    }

    @Override
    public boolean hasGoal(String key, ConfigurationSection sec) {
        if (!sec.contains("stat")) {
            return false;
        }
        String name = sec.getString("stat");
        Stat stat = sapi.getStat(name);
        if (stat == null) {
            String statName = (name.substring(0, 1).toUpperCase() + name.substring(1).toLowerCase()).replace("_", " ");
            stat = sapi.getStat(statName);
        }
        return stat != null;
    }

    @Override
    public Goal getGoal(String key, ConfigurationSection section) {
        Stat stat = this.sapi.getStat(section.getString("stat"));
        double amount = section.getDouble("amount");
        StatsGoal goal = new StatsGoal(sapi, stat, amount);
        if (section.contains("type")) {
            goal.setGlobal(section.getString("type").equalsIgnoreCase("total") || section.getString("type").equalsIgnoreCase("global"));
        }
        if (section.contains("vars")) {
            Object[] vars = section.getString("vars").split(" ");
            goal.setVariables(vars);
        }
        return goal;
    }

    @Override
    public boolean hasReward(String key) {
        return false;
    }

    @Override
    public Reward getReward(String key, ConfigurationSection section) {
        return null;
    }

    @Override
    public boolean hasCompletion(String key) {
        return false;
    }

    @Override
    public Completion getCompletion(String key, ConfigurationSection section) {
        return null;
    }

}
