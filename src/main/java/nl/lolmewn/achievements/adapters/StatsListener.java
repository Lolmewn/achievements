package nl.lolmewn.achievements.adapters;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import nl.lolmewn.achievements.Achievement;
import nl.lolmewn.achievements.AchievementsAPI;
import nl.lolmewn.achievements.EventListener;
import nl.lolmewn.achievements.api.AchievementPlayerLoadedEvent;
import nl.lolmewn.achievements.api.Goal;
import nl.lolmewn.achievements.goal.StatsGoal;
import nl.lolmewn.achievements.player.AchievementPlayer;
import nl.lolmewn.stats.api.StatUpdateEvent;
import org.bukkit.OfflinePlayer;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;

/**
 * @author Sybren
 */
public class StatsListener implements Listener {

    private final Plugin plugin;
    private final AchievementsAPI api;
    private final HashMap<String, HashSet<StatUpdateEvent>> playerLoadWait = new HashMap<>();

    public StatsListener(Plugin plugin, AchievementsAPI api) {
        this.plugin = plugin;
        this.api = api;
    }

    @EventHandler(ignoreCancelled = true, priority = EventPriority.MONITOR)
    public void onStatUpdate(final StatUpdateEvent event) throws Exception {
        OfflinePlayer player = plugin.getServer().getOfflinePlayer(event.getPlayer().getPlayername());
        AchievementPlayer aPlayer = api.getPlayer(player.getUniqueId());
        if (aPlayer == null) {
            //not loaded yet
            if (this.playerLoadWait.containsKey(event.getPlayer().getPlayername())) {
                this.playerLoadWait.get(event.getPlayer().getPlayername()).add(event);
            } else {
                this.playerLoadWait.put(event.getPlayer().getPlayername(), new HashSet<StatUpdateEvent>() {
                    {
                        this.add(event);
                    }
                });
            }
            return;
        }
        for (Achievement ach : api.getAchievements()) {
            if (aPlayer.hasCompletedAchievement(ach.getId())) {
                continue;
            }
            if (!meetsGoals(aPlayer, event, ach)) {
                continue;
            }
            ach.achievementGet(plugin, aPlayer);
        }
    }

    public boolean meetsGoals(AchievementPlayer ap, StatUpdateEvent event, Achievement ach) {
        for (Goal g : ach.getGoals()) {
            if (g instanceof StatsGoal) {
                if (!meetsStatsGoal(ap, event, (StatsGoal) g)) {
                    return false;
                }
            } else {
                if (g.getProgress(ap) < 1) {
                    return false;
                }
            }
        }
        return true;
    }

    private boolean meetsStatsGoal(AchievementPlayer ap, StatUpdateEvent event, StatsGoal g) {
        if (!event.getStat().equals(g.getStat())) {
            return g.getProgress(ap)>= 1;
        }
        if (g.isGlobal()) {
            int totalValue = 0;
            for (Object[] vars : event.getStatData().getAllVariables()) {
                totalValue += event.getStatData().getValue(vars);
            }
            totalValue += event.getUpdateValue();
            if (g.getAmount() <= totalValue) {
                return true;
            }
        } else {
            if (!Arrays.toString(event.getVars()).equalsIgnoreCase(Arrays.toString(g.getVariables()))) {
                return false;
            }
            if (event.getNewValue() >= g.getAmount()) {
                return true;
            }
        }
        return false;
    }

    @EventHandler
    public void onLoadComplete(AchievementPlayerLoadedEvent event) {
        if (this.playerLoadWait.containsKey(event.getPlayer().getName())) {
            for (StatUpdateEvent ev : this.playerLoadWait.get(event.getPlayer().getName())) {
                try {
                    this.onStatUpdate(ev);
                } catch (Exception ex) {
                    Logger.getLogger(EventListener.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            this.playerLoadWait.remove(event.getPlayer().getName());
        }
    }
}
