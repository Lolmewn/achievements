package nl.lolmewn.achievements.adapters;

import java.util.HashSet;
import nl.lolmewn.achievements.AchievementsAPI;
import nl.lolmewn.achievements.api.AchievementAdapter;
import nl.lolmewn.achievements.api.Completion;
import nl.lolmewn.achievements.api.Goal;
import nl.lolmewn.achievements.api.Reward;
import nl.lolmewn.achievements.completion.Broadcast;
import nl.lolmewn.achievements.completion.Messages;
import nl.lolmewn.achievements.reward.Commands;
import nl.lolmewn.achievements.reward.ConsoleCommand;
import nl.lolmewn.achievements.reward.Items;
import nl.lolmewn.achievements.reward.Money;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.plugin.Plugin;

/**
 * @author Sybren
 */
public class DefaultAdapter implements AchievementAdapter{

    private final Plugin plugin;
    private final AchievementsAPI api;
    private final HashSet<String> rewards = new HashSet<String>(){{
        this.add("money");
        this.add("items");
        this.add("commands");
        this.add("consoleCommands");
    }};
    private final HashSet<String> comp = new HashSet<String>(){{
        this.add("message");
        this.add("broadcast");
    }};

    public DefaultAdapter(Plugin plugin, AchievementsAPI api) {
        this.plugin = plugin;
        this.api = api;
    }
    
    @Override
    public Plugin getPlugin() {
        return plugin;
    }

    @Override
    public AchievementsAPI getAPI() {
        return api;
    }

    @Override
    public boolean hasGoal(String key, ConfigurationSection sec) {
        return false;
    }

    @Override
    public Goal getGoal(String key, ConfigurationSection section) {
        return null; //there are no goals anyway
    }

    @Override
    public boolean hasReward(String key) {
        return this.rewards.contains(key);
    }

    @Override
    public Reward getReward(String key, ConfigurationSection section) {
        if(key.equals("money")){
            return new Money(section.getDouble(key), getPlugin());
        }
        if(key.equals("items")){
            return new Items(section.getString(key));
        }
        if(key.equals("commands")){
            return new Commands(section.getStringList(key));
        }
        if(key.equalsIgnoreCase("consoleCommands")){
            return new ConsoleCommand(section.getStringList(key));
        }
        return null;
    }

    @Override
    public boolean hasCompletion(String key) {
        return this.comp.contains(key);
    }

    @Override
    public Completion getCompletion(String key, ConfigurationSection section) {
        if(key.equals("message")){
            return new Messages(section.getStringList(key));
        }
        if(key.equals("broadcast")){
            return new Broadcast(section.getString(key));
        }
        return null;
    }

}
