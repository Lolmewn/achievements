/*
 *  Copyright 2013 Lolmewn .
 */

package nl.lolmewn.achievements.api;

import nl.lolmewn.achievements.Achievement;
import nl.lolmewn.achievements.player.AchievementPlayer;

/**
 *
 * @author Lolmewn 
 */
public interface Completion {

    public void onCompletion(AchievementPlayer player, Achievement ach);
    
}
