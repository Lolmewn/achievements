package nl.lolmewn.achievements.api;

import java.util.HashSet;
import java.util.List;
import nl.lolmewn.achievements.Achievement;

/**
 * @author Sybren
 */
public class AchievementBuilder {

    private final Achievement ach;
    
    public AchievementBuilder(int id){
        this.ach = new Achievement(id);
    }

    public AchievementBuilder addCompletion(Completion comp) {
        ach.addCompletion(comp);
        return this;
    }

    public AchievementBuilder addGoal(Goal goal) {
        ach.addGoal(goal);
        return this;
    }

    public AchievementBuilder addReward(Reward reward) {
        ach.addReward(reward);
        return this;
    }
    
    public Achievement build(){
        return this.ach;
    }

    public AchievementBuilder setName(String name) {
        ach.setName(name);
        return this;
    }

    public AchievementBuilder setDescription(String description) {
        ach.setDescription(description);
        return this;
    }

    public AchievementBuilder setPermissionNode(String permissionNode) {
        ach.setPermissionNode(permissionNode);
        return this;
    }
    
}
