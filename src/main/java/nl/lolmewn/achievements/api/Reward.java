/*
 *  Copyright 2013 Lolmewn .
 */

package nl.lolmewn.achievements.api;

import nl.lolmewn.achievements.player.AchievementPlayer;

/**
 *
 * @author Lolmewn 
 */
public interface Reward {
    
    public void give(AchievementPlayer player);

}
