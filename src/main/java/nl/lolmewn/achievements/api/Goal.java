package nl.lolmewn.achievements.api;

import nl.lolmewn.achievements.player.AchievementPlayer;

/**
 *
 * @author Lolmewn
 */
public interface Goal {
    
    public String getType();

    public String getDisplayName();
    public String getDisplayObjective();

    /**
     * Calculate the progress of the given player for this goal
     * @param player Player to calculate progress for
     * @return progress in percent, between 0 and 1, 0 being no progress and 1 being completed.
     */
    public double getProgress(AchievementPlayer player);
}
