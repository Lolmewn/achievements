package nl.lolmewn.achievements.api;

import nl.lolmewn.achievements.AchievementsAPI;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.plugin.Plugin;

/**
 * @author Lolmewn
 */
public interface AchievementAdapter {
    
    public Plugin getPlugin();
    
    public AchievementsAPI getAPI();
    
    public boolean hasGoal(String key, ConfigurationSection section);
    public Goal getGoal(String key, ConfigurationSection section);
    public boolean hasReward(String key);
    public Reward getReward(String key, ConfigurationSection section);
    public boolean hasCompletion(String key);
    public Completion getCompletion(String key, ConfigurationSection section);

}
