/*
 *  Copyright 2013 Lolmewn .
 */
package nl.lolmewn.achievements;

import java.util.ArrayList;
import java.util.List;
import nl.lolmewn.achievements.api.AchievementGetEvent;
import nl.lolmewn.achievements.api.Completion;
import nl.lolmewn.achievements.api.Goal;
import nl.lolmewn.achievements.api.Reward;
import nl.lolmewn.achievements.player.AchievementPlayer;
import org.bukkit.plugin.Plugin;

/**
 *
 * @author Lolmewn 
 */
public class Achievement {

    private final int id;
    private String name;
    private String description;
    private String permissionNode;
    private final List<Goal> goals = new ArrayList<>();
    private final List<Reward> rewards = new ArrayList<>();
    private final List<Completion> completions = new ArrayList<>();

    @Deprecated
    public Achievement(Main main, int id) {
        this(id);
    }

    public Achievement(int id) {
        this.id = id;
    }

    public void addGoal(Goal goal) {
        this.goals.add(goal);
    }

    public void addReward(Reward reward) {
        this.rewards.add(reward);
    }

    public void addCompletion(Completion comp) {
        this.completions.add(comp);
    }

    public List<Goal> getGoals() {
        return goals;
    }

    public List<Reward> getRewards() {
        return rewards;
    }

    public List<Completion> getCompletions() {
        return this.completions;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return this.id;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean hasPermissionNode(String node) {
        return this.permissionNode != null;
    }

    public String getPermissionNode() {
        return permissionNode;
    }

    public void setPermissionNode(String permissionNode) {
        this.permissionNode = permissionNode;
    }

    public void achievementGet(Plugin plugin, AchievementPlayer ap) {
        ap.markAsCompleted(this.getId());
        AchievementGetEvent ae = new AchievementGetEvent(this, ap);
        plugin.getServer().getPluginManager().callEvent(ae);
        for (Reward reward : this.getRewards()) {
            reward.give(ap);
        }
        for (Completion com : this.getCompletions()) {
            com.onCompletion(ap, this);
        }
    }
}
