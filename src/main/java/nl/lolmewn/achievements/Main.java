package nl.lolmewn.achievements;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import nl.lolmewn.achievements.Updater.UpdateType;
import nl.lolmewn.achievements.adapters.DefaultAdapter;
import nl.lolmewn.achievements.adapters.StatsAdapter;
import nl.lolmewn.achievements.mysql.DefaultProvider;
import nl.lolmewn.achievements.mysql.IDSyncer;
import nl.lolmewn.achievements.mysql.MySQLProvider;
import nl.lolmewn.achievements.mysql.StatsProvider;
import nl.lolmewn.achievements.player.PlayerManager;
import nl.lolmewn.stats.api.StatsAPI;
import nl.lolmewn.stats.api.mysql.MySQLAttribute;
import nl.lolmewn.stats.api.mysql.MySQLType;
import nl.lolmewn.stats.api.mysql.StatsTable;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.ServicePriority;
import org.bukkit.plugin.java.JavaPlugin;
import org.mcstats.Metrics;
import org.mcstats.Metrics.Graph;
import org.mcstats.Metrics.Plotter;

public class Main extends JavaPlugin {

    private StatsAPI statsAPI;
    private AchievementsAPI achAPI;
    private Settings settings;
    private AchievementManager aManager;
    private PlayerManager playerManager;
    private MySQLProvider mysqlProvider;

    protected double newVersion;

    @Override
    public void onDisable() {
        for (UUID player : playerManager.getPlayers()) {
            this.playerManager.savePlayer(player, true);
        }
    }

    @Override
    public void onEnable() {
        this.settings = new Settings(this);
        this.settings.checkExistance();
        this.settings.loadConfig();
        this.registerAPI();
        Plugin stats = this.getServer().getPluginManager().getPlugin("Stats");
        if (stats == null) {
            this.getLogger().warning("Stats not found, switching to own MySQL implementation!");
            this.getLogger().warning("It is highly recommended to use Stats, which you can find here: http://dev.bukkit.org/server-mods/lolmewnstats/");
            this.mysqlProvider = new DefaultProvider(this);
        } else {
            if (!stats.isEnabled()) {
                this.getLogger().severe("Stats plugin has been disabled, Achievements cannot start!");
                this.getLogger().severe("Please resolve any Stats issues first!");
                this.getServer().getPluginManager().disablePlugin(this);
                return;
            }
            if (!stats.getDescription().getVersion().startsWith("2")) {
                this.getLogger().severe("Incompatible Stats version found!");
                this.getLogger().severe("Please update stats to 2.0.0 or higher.");
                this.getLogger().severe("Do this here: http://dev.bukkit.org/server-mods/lolmewnstats/files/");
                this.getServer().getPluginManager().disablePlugin(this);
                return;
            }
            this.statsAPI = getServer().getServicesManager().getRegistration(nl.lolmewn.stats.api.StatsAPI.class).getProvider();
            this.achAPI.addAdapter(new StatsAdapter(this, achAPI, statsAPI));
        }
        this.achAPI.addAdapter(new DefaultAdapter(this, achAPI));
        try {
            this.loadMySQL();
        } catch (SQLException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            this.getLogger().severe("The error above says something failed while trying to set up MySQL!");
            this.getLogger().severe("The plugin cannot continue to function. Please fix the error above first!");
            this.getServer().getPluginManager().disablePlugin(this);
            return;
        }
        try {
            this.playerManager = new PlayerManager(this);
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.loadOnlinePlayers();
        this.aManager = new AchievementManager(this);
        try {
            this.aManager.loadAchievements();
        } catch (InvalidConfigurationException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.getServer().getPluginManager().registerEvents(new EventListener(this), this);
        try {
            Metrics m = new Metrics(this);
            Graph g = m.createGraph("Stats");
            Plotter p = new Plotter() {
                @Override
                public String getColumnName() {
                    return "Amount of achievements";
                }

                @Override
                public int getValue() {
                    return getAchievementManager().getAchievements().size();
                }
            };
            g.addPlotter(p);
            m.addGraph(g);
            m.start();
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (this.getSettings().isUpdate()) {
            new Updater(this, 55920, this.getFile(), UpdateType.DEFAULT, false);
        }

        this.getCommand("achievements").setExecutor(new CommandHandler(this));

        this.scheduleDataSaver();
    }

    public Settings getSettings() {
        return settings;
    }

    public AchievementsAPI getAPI() {
        return this.achAPI;
    }

    public StatsAPI getStatsAPI() {
        return statsAPI;
    }

    public AchievementManager getAchievementManager() {
        return aManager;
    }

    public PlayerManager getPlayerManager() {
        return this.playerManager;
    }

    public MySQLProvider getMysqlProvider() {
        return mysqlProvider;
    }

    public void debug(String message) {
        if (this.getSettings().isDebug()) {
            this.getLogger().info("[Debug] " + message);
        }
    }

    public void loadOnlinePlayers() {
        for (Player p : this.getServer().getOnlinePlayers()) {
            this.playerManager.loadPlayer(p.getUniqueId());
        }
    }

    public boolean hasStats() {
        return this.statsAPI != null;
    }

    private void scheduleDataSaver() {
        this.getServer().getScheduler().runTaskTimerAsynchronously(this, new Runnable() {

            @Override
            public void run() {
                for (UUID player : playerManager.getPlayers()) {
                    playerManager.savePlayer(player, false);
                }
            }
        }, 6000, 6000);
        this.getServer().getScheduler().runTaskTimerAsynchronously(this, new IDSyncer(this), 600, 36000);
    }

    private void registerAPI() {
        this.achAPI = new AchievementsAPI(this);
        this.getServer().getServicesManager().register(AchievementsAPI.class, this.achAPI, this, ServicePriority.Low);
    }

    private void loadMySQL() throws SQLException {
        if (this.hasStats()) {
            this.mysqlProvider = new StatsProvider(statsAPI);
        } else {
            this.mysqlProvider = new DefaultProvider(this);
        }
        final String tableName = this.getMysqlProvider().getPrefix() + "achievements";
        if (this.hasStats()) {
            if (!this.statsAPI.getStatsTableManager().containsKey(tableName)) {
                statsAPI.getStatsTableManager().put(tableName, new StatsTable(tableName, false, statsAPI.isCreatingSnapshots()));
            }
            StatsTable table = statsAPI.getStatsTable(tableName);
            table.addColumn("id", MySQLType.INTEGER).addAttributes(MySQLAttribute.AUTO_INCREMENT, MySQLAttribute.NOT_NULL, MySQLAttribute.PRIMARY_KEY);
            table.addColumn("player_id", MySQLType.INTEGER).addAttributes(MySQLAttribute.NOT_NULL);
            table.addColumn("achievement_id", MySQLType.INTEGER).addAttributes(MySQLAttribute.NOT_NULL);
        } else if (!this.mysqlProvider.hasTable(tableName)) {
            Connection con = this.mysqlProvider.getConnection();
            Statement st = con.createStatement();
            st.execute("CREATE TABLE " + tableName + " ("
                    + "  id int(11) NOT NULL AUTO_INCREMENT,"
                    + "  player_id int(11) NOT NULL,"
                    + "  achievement_id int(11) NOT NULL,"
                    + "  PRIMARY KEY (id)"
                    + ") ENGINE=InnoDB DEFAULT CHARSET=utf8;");
            st.close();
        }
    }
}
